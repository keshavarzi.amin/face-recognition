function [i,s] =DWT2_me(im);
% [LL,LH,HL,HH]=dwt2(im,'haar');
[c,s]=wavedec2(im,1,'haar');
s1=s(1,1);
s2=s(1,2);
LL3=c(1:s1*s2);
i=[LL3(:)']';