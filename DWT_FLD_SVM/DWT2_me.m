function [i,s] =DWT2_me(im);
% [LL,LH,HL,HH]=dwt2(im,'haar');
[c,s]=wavedec2(im,3,'haar');
s1=s(1);
LL3=c(1:s1*s1);
i=[LL3(:)']';