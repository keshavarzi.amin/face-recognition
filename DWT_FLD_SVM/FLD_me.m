function[vec_fld]=FLD_me(x,num_xclass,Ni,xmean);
% Ni is number of data in the class

[vec]=PCA_me(x,xmean); %find pca eigenvectors  of all input data


N=size(x,2);  % number of input data
 

 
Sb=zeros(size(x,1));
Sw=zeros(size(x,1));
zk=[];
mean_xclass=[];
for i= 1:num_xclass
      mean_xclass(:,i)=mean(x(:,(i-1)*Ni+1:(i)*Ni),2);%  find means of input data classes
    Sb=Sb+(  Ni*(mean_xclass(:,i)-xmean)*(mean_xclass(:,i)-xmean)');% find between-class scatter matrix
    zk=x(:,((i-1)*Ni+1):(i*Ni));
    for k=1:Ni
         Sw=Sw+((zk(:,k)-mean_xclass(:,i))*(zk(:,k)-mean_xclass(:,i))'); %find  within-class scatter matrix
    end
end
Sw=(1/N)*Sw;
Sb=(1/N)*Sb;

%  find new Sw and Sb
vec_pca=vec(:,1:39);

Sw_new=vec_pca'*Sw*vec_pca;
Sb_new=vec_pca'*Sb*vec_pca;
S_opt=(inv(Sw_new))*Sb_new; 

[V U]=eig(S_opt);
[eig_val,num_val]=sort(diag(U),'descend');
vec_lda1=V(:,num_val);
vec_lda=vec_lda1(:,1:39);
vec_fld=vec_lda'*vec_pca';
vec_fld=vec_fld';

