function Percent=Percent_me (input1)

[m,n]=size(input1);
p=0;

for i=1:m
    for j=1:n
        if input1(i,j)==i
            p=p+1;
        end
    end
end
Percent=p/(m*n);
Percent=100*Percent;