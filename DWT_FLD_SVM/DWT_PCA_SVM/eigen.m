function [ eig_vec]=eigen(imagetrain,numclass,num_imtrain)
  immean=mean(imagetrain,2);
 eig_vec=PCA_me(imagetrain,immean);
 