function [eigen_vectors]=PCA_me(x,xmean)
% x is input images matrix
% xmean is mean of x
N=size(x,2);
phi=x-repmat(xmean,1,N);
 L=cov(x);

[V D]=eig(L); % V is eigenvector of L and D is eigenvalues
[eigen_values,num_values]=sort(diag(D),'descend');
eigen_vectors=V(:,num_values);
 eigen_vectors=phi*eigen_vectors;
% principal_component=b'*x;