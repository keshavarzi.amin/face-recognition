function[imtrain_new,imtest_new] =eigen_new (imagetrain,imagetest,eig_vec,num_eignew)
eig_facenew=eig_vec(:,1:num_eignew);
 imtrain_new=eig_facenew'*imagetrain;
 imtest_new= eig_facenew'*imagetest;