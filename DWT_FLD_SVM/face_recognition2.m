% clc
clear
num_allfaces=400;
num_onefaces=10;
num_imtrain=6;%num_eignew=6;
eig_facenew=[]; images=[];n=1;nn=1;
 numclass=num_allfaces/num_onefaces;
 
  add_imtrain='D:\payan nameh\database\ORL\ff';
  fileimages=dir(fullfile(add_imtrain,'*.pgm'));
 
  for i=1:num_allfaces
       im=imread(fullfile(add_imtrain,fileimages(i,1).name));
       
        [images(:,i),s]=DWT2_me(im);
        
%         images(:,i)=im2double(im(:));
  end
  gro=ones(1,num_imtrain);
            
 for i=2:numclass
  gro=[gro i*ones(1,num_imtrain)];
 end
  rand_imsample1=[2; 4 ;5; 7; 8 ;6];
 rand_imsample2=[2 ;5 ;6; 4; 8; 10];
 rand_imsample3=[1 ;2; 3; 5; 8; 10];
 
[imtrain1,imtest1]=rand_select_images(images,num_imtrain,num_allfaces,num_onefaces,rand_imsample1);
[imtrain2,imtest2]=rand_select_images(images,num_imtrain,num_allfaces,num_onefaces,rand_imsample2);
[imtrain3,imtest3]=rand_select_images(images,num_imtrain,num_allfaces,num_onefaces,rand_imsample3);
  
 
 [ eig_vec1]=eigen(imtrain1, numclass,num_imtrain);
 [ eig_vec2]=eigen(imtrain2, numclass,num_imtrain);
 [ eig_vec3]=eigen(imtrain3, numclass,num_imtrain);
 
 rate=zeros(40,1);
 for num_eignew=1:39
 
  [ imtrain_new1,imtest_new1]=eigen_new(imtrain1,imtest1,eig_vec1,num_eignew);
  [ imtrain_new2,imtest_new2]=eigen_new(imtrain2,imtest2,eig_vec2,num_eignew);
  [ imtrain_new3,imtest_new3]=eigen_new(imtrain3,imtest3,eig_vec3,num_eignew);
 
  
 
 
 result1=multisvm(imtrain_new1,gro,imtest_new1');
 result2=multisvm(imtrain_new2,gro,imtest_new2');
 result3=multisvm(imtrain_new3,gro,imtest_new3');
 
 
 r1=reshape(result1,4,numclass);
 r2=reshape(result2,4,numclass);
 r3=reshape(result3,4,numclass);
%  r4=reshape(result4,num_imtrain,numclass);
 
 r1=r1';r2=r2';r3=r3';

 
 Percent1=Percent_me(r1);
 Percent2=Percent_me(r2);
 Percent3=Percent_me(r3);
%  Percent4=Percent_me(r4);
 
 Percent_total=Percent1+Percent2+Percent3;
 
 num_eignew

 Percent_total=Percent_total/3
 rate(num_eignew,1)=Percent_total;
 
 end



