function [rand_imtrain,rand_imtest]=rand_select_images(images,num_rand,num_allfaces,num_onefaces,rand_imsample)
% rand_imsample=randsample(num_onefaces,num_rand);
sample_imtrain=[];
sample_imtest=[];
for i=1:(num_allfaces/num_onefaces)
    one_faces=images(:,(((i-1)*num_onefaces+1)):(i*num_onefaces));
    sample_imtrainone=one_faces(:,rand_imsample);
    sample_imtrain=[sample_imtrain sample_imtrainone];
    im=one_faces;
    im(:,rand_imsample)=[];
    sample_imtest=[sample_imtest im];
    
end
    rand_imtrain=sample_imtrain;
    rand_imtest=sample_imtest;